import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.MainPage;
import utilities.base.BaseTestGlobal;
import utilities.dataProviders.DataProviders;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static utilities.links.UrlLinks.YAHOO_MAIN;

/**
 * @author Andrey B
 */
public class CheckLoginForm extends BaseTestGlobal {
    private MainPage mainPage;
    private LoginPage loginPage;


    @Test
    public void loginFormShouldBeOpened(){
        mainPage = new MainPage();
        open(YAHOO_MAIN);
        mainPage.
                clickLoginButton().
                getLoginForm().
                shouldBe(visible);
    }

    @Test
    public void loginFormComponentsShouldBePresent(){
        mainPage = new MainPage();
        loginPage = new LoginPage();
        open(YAHOO_MAIN);
        mainPage.clickLoginButton();
        loginPage.getLoginFormLogo().shouldBe(visible);
        loginPage.getLoginFormNextButton().shouldBe(visible);
        loginPage.getLoginFormLoginInput().shouldBe(visible);
        loginPage.getLoginFormStaySignIn().shouldBe(visible);
        loginPage.getLoginFormTroubleSigningIn().shouldBe(visible);
        loginPage.getLoginFormSignUp().shouldBe(visible);
    }

    @Test(dataProvider = "incorrectEmails", dataProviderClass = DataProviders.class)
    public void errorMessageShouldAppearsWithInvalidEmail(String incorrectEmail){
        mainPage = new MainPage();
        loginPage = new LoginPage();
        open(YAHOO_MAIN);
        mainPage.
                clickLoginButton().
                doLogin(incorrectEmail);
        loginPage.getLoginFormUserErrorMessage().shouldBe(visible);
    }

    @Test(dataProvider = "correctEmails", dataProviderClass = DataProviders.class)
    public void passwordFormShouldAppearsWithValidEmail(String correctEmail){
        mainPage = new MainPage();
        loginPage = new LoginPage();
        open(YAHOO_MAIN);
        mainPage.
                clickLoginButton().
                doLogin(correctEmail);
        loginPage.getLoginFormUserNameGreeting().shouldHave(text("Hello" + correctEmail));
    }
}
