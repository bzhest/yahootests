import com.codeborne.selenide.Condition;
import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.MainPage;
import utilities.base.BaseTestGlobal;
import utilities.links.UrlLinks;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static utilities.links.UrlLinks.EMAIL;
import static utilities.links.UrlLinks.PASSWORD;
import static utilities.links.UrlLinks.YAHOO_MAIN;

/**
 * @author Andrey B.
 */
public class CheckOpenMailPageUnderLoggedInUser extends BaseTestGlobal {

    private MainPage mainPage;
    private LoginPage loginPage;

    @Test
    public void loggedInUserCanOpenMailPage(){
        mainPage = new MainPage();
        loginPage = new LoginPage();
        open(YAHOO_MAIN);
        mainPage.
                clickLoginButton().
                doLogin(EMAIL);
        loginPage.getLoginFormUserNameGreeting().shouldHave(text("Hello " + EMAIL));
        loginPage.
                doPassword(PASSWORD).
                clickMailButton().
                getYahooMailTab().shouldBe(visible);
        loginPage.getLoginFormUserNameGreeting().shouldNotHave(text("Hello " + EMAIL));

    }

}
