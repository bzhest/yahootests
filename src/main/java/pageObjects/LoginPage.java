package pageObjects;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

/**
 * @author Andrey B.
 */
@Getter
public class LoginPage {

    private By byLoginForm = By.cssSelector(".login-box");
    private By byFormLogo = By.cssSelector(".login-logo .logo");
    private By byNextButton = By.cssSelector("#login-signin");
    private By byLoginUserInput = By.cssSelector("#login-username");
    private By byLoginPasswordInput = By.cssSelector("#login-passwd");
    private By byLoginStaySignIn = By.cssSelector(".stay-signed-in");
    private By byLoginTroubleSigningIn = By.cssSelector("#mbr-forgot-link");
    private By byLoginSignUp = By.cssSelector(".row.sign-up");
    private By byUserNameErrorMessage = By.cssSelector("#username-error");
    private By byUserNameGreeting = By.cssSelector(".username");

    private SelenideElement loginForm = $(byLoginForm);
    private SelenideElement loginFormLogo = $(byFormLogo);
    private SelenideElement loginFormNextButton = $(byNextButton);
    private SelenideElement loginFormLoginInput = $(byLoginUserInput);
    private SelenideElement loginFormPasswordInput = $(byLoginPasswordInput);
    private SelenideElement loginFormStaySignIn = $(byLoginStaySignIn);
    private SelenideElement loginFormTroubleSigningIn = $(byLoginTroubleSigningIn);
    private SelenideElement loginFormSignUp = $(byLoginSignUp);
    private SelenideElement loginFormUserErrorMessage = $(byUserNameErrorMessage);
    private SelenideElement loginFormUserNameGreeting = $(byUserNameGreeting);

    public LoginPage clickNextButton(){
        loginFormNextButton.click();
        return page(LoginPage.class);
    }
    public LoginPage doLogin(String email){
        loginFormLoginInput.setValue(email);
        clickNextButton();
        return page(LoginPage.class);
    }

    public MainPage doPassword(String password){
        loginFormPasswordInput.setValue(password);
        clickNextButton();
        return page(MainPage.class);
    }
}
