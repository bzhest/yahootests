package pageObjects;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

/**
 * @author Andrey B.
 */
@Getter
public class MainPage {

    By bySignInButton = By.cssSelector("#uh-signin");
    By bySignOutButton = By.cssSelector("#uh-signout");
    By byMailButton = By.cssSelector("#uh-mail-link");
    By byProfileButton = By.cssSelector("button[title='Profile']");
    By byLoggedInUserEmail = By.cssSelector("#uh-as-email");

    SelenideElement userEmail = $(byLoggedInUserEmail);

    public LoginPage clickLoginButton(){
        $(bySignInButton).click();
        return page(LoginPage.class);
    }

    public MailPage clickMailButton(){
        $(byMailButton).click();
        return page(MailPage.class);
    }

    public MainPage hoverProfileButton(){
        $(byProfileButton).hover();
        return page(MainPage.class);
    }

    public MainPage clickSignOutButton(){
        $(bySignOutButton).click();
        return this;
    }
}
