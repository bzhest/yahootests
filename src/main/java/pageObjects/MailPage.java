package pageObjects;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;
import utilities.base.BaseTestGlobal;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$$;

/**
 * @author Andrey B.
 */
@Getter
public class MailPage extends BaseTestGlobal {

    By byHeaderTabsList = By.cssSelector("td[role='presentation']");

    SelenideElement yahooMailTab = $$(byHeaderTabsList).find(text("Yahoo Mail"));
}
