package utilities.dataProviders;

import org.testng.annotations.DataProvider;

/**
 * @author Andrey B.
 */
public class DataProviders {

    @DataProvider
    public static Object[][] incorrectEmails(){
        return new Object[][]{
                {""},{"NotAnEmail"},{"@NotAnEmail"},
                {"\"\"test\\blah\"\"@example.com"}
        };
    }

    @DataProvider
    public static Object[][] correctEmails(){
        return new Object[][]{
                {"test@gmail.com"},{"test1@gmail.com"},{"bzhest@gmail.com"},
        };
    }
}
