package utilities.base;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.testng.SoftAsserts;
import io.qameta.allure.Attachment;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.Set;

import static com.codeborne.selenide.Configuration.AssertionMode.SOFT;

/**
 * @author Andrey B.
 */
@Listeners(SoftAsserts.class)
public abstract class BaseTestGlobal {

    protected static WebDriver driver = null;
    protected static Set<Cookie> cookie = null;

    public WebDriver getDriver(){
        if(driver == null){
            return WebDriverRunner.getWebDriver();
        }
        return driver;
    }

    @BeforeTest
    @Parameters("browserName")
    public void setUp(@Optional("chrome") String browser) throws IOException {
        Configuration.browser = "chrome";
        Configuration.timeout = 10000;
        Configuration.pageLoadStrategy = "normal";
        Configuration.assertionMode = SOFT;
        Configuration.reportsFolder = "test-result/reports";
        Configuration.browserSize = "1920x1080";
        driver = getDriver();
    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            createAttachment();
        }
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    protected byte[] createAttachment() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

}
